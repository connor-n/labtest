﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ToPlay : MonoBehaviour
{
    public void switchScene(string sceneName)
    {
        SceneManager.LoadScene (sceneName);
    }

    public void quitMe()
    {
        Application.Quit();
    }
}
