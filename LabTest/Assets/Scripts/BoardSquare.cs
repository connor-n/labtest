﻿using UnityEngine;
using System.Collections;

public class BoardSquare : MonoBehaviour
{
	public int timeBonus {get; set;}
	public bool toTest = false;  //indicates whether the square needs to be tested against a another
	public bool found = false; //indicates whether the square has already been matched

	protected Texture2D[] images = new Texture2D[3];
	protected GameObject gameManager;
	protected GameObject textBox;

	private Color color = new Color(1.0f,0.0f,0.0f,0.25f);
	

	protected virtual void Awake ()
	{
		images [0] = Resources.Load ("Square") as Texture2D;
		images [1] = Resources.Load ("Square") as Texture2D;
		images [2] = Resources.Load ("Square") as Texture2D;


		timeBonus = 2; //sets a default timeBonus
	}

	protected virtual void Start() {
		this.GetComponent<Renderer>().material.mainTexture = images [0];
		gameManager = GameObject.Find("GameController"); 
		textBox = this.transform.Find("TextBox").gameObject;
		textBox.GetComponent<Renderer>().enabled = false; //turns text box visibility off
	}

	protected virtual void OnMouseDown ()
	{
		gameManager.GetComponent<GameController>().squaresSelected++;
		this.GetComponent<Renderer>().material.mainTexture = images [1];
		this.GetComponent<Collider>().enabled = false;
		toTest = true;
		onSquareSelected ();
	}

	protected virtual void onSquareSelected ()
	{
		textBox.GetComponent<Renderer>().enabled = true;
		gameManager.GetComponent<GameController>().checkForPair();	//all checking for pairs happens in GameManager script
	}

	//called from GameManager when a pair is matched
	public void changeSquareStatus(Color color, string str){		
		this.color = color;
		textBox.GetComponent<TextMesh>().text = str;
		textBox.GetComponent<TextMesh>().color = new Color(0.9f,0.9f,0.9f);
		updateEndGraphic();		
	}

	//changes square to found status
	protected virtual void updateEndGraphic ()
	{
		this.textBox.GetComponent<Renderer>().enabled = true;
		this.toTest=false;
		this.found = true;
		this.GetComponent<Renderer>().material.color = color;
		this.GetComponent<Renderer>().material.mainTexture = images [2];	 
		textBox.transform.Translate(new Vector3(0,0.5f,0));
	}
	
	public virtual void resetSquare ()
	{	
		textBox.GetComponent<Renderer>().enabled = false;
		this.toTest = false;
		this.GetComponent<Renderer>().material.mainTexture = images [0];
		this.GetComponent<Collider>().enabled = true;
	}

	public void showSquare() {
		textBox.GetComponent<Renderer>().enabled = true;
		this.GetComponent<Renderer>().material.mainTexture = images [1];
	}

}
